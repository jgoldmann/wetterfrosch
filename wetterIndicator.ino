/*
  Arduino Starter Kit example
 Project 5  - Servo Mood Indicator
 
 This sketch is written to accompany Project 5 in the
 Arduino Starter Kit
 
 Parts required:
 servo motor 
 10 kilohm potentiometer 
 2 100 uF electrolytic capacitors
 
 Created 13 September 2012
 by Scott Fitzgerald
 
 http://arduino.cc/starterKit
 
 This example code is part of the public domain 
 */

// include the servo library
#include <Servo.h>

Servo myServo;  // create a servo object 

int const potPin = A0; // analog pin used to connect the potentiometer
int potVal;  // variable to read the value from the analog pin 
int angle;   // variable to hold the angle for the servo motor 
char myserial;
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

void setup() {
  myServo.attach(9); // attaches the servo on pin 9 to the servo object 
  Serial.begin(9600); // open a serial connection to your computer
  potVal = 0;
  inputString.reserve(200);
}

void loop() {
  if (stringComplete) {
    Serial.println(inputString);
    // clear the string:
    if(inputString=="good\n")
    {
      potVal = 1000;
    }
    if(inputString=="bad\n")
    {
      potVal = 0;
    }
    if(inputString=="same\n")
    {
      potVal = 500;
    }
    inputString = "";
    stringComplete = false;
  }

  // scale the numbers from the pot 
  angle = map(potVal, 0, 1023, 0, 179);

  // set the servo position  
  myServo.write(angle);

  // wait for the servo to get there 
  delay(15);
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

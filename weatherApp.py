import serial
import forecastio
import datetime


class weatherApp(object):
    def __init__(self):
        self.api_key = "b373c9329858e58db0e419465bc3b7d1"
        self.lat = 51.842054
        self.lng = 5.859681
        self.ser = serial.Serial('/dev/ttyACM0', 9600)
    
    def callArduino(self, status):
        if status == -1:
            self.ser.write('bad\n')
        elif status == 0:
            self.ser.write('same\n')
        elif status == 1:
            self.ser.write('good\n')
            
    def callWeather(self):
        heute = datetime.datetime.today()
        gestern = datetime.datetime.today() - datetime.timedelta(days=1)
        
        todayforecast = forecastio.load_forecast(self.api_key, self.lat, self.lng, time=heute)
        yesterdayforecast = forecastio.load_forecast(self.api_key, self.lat, self.lng, time=gestern,units='si')
        
        apparentTempYesterday = yesterdayforecast.currently().d['apparentTemperature']
        apparentTempToday = todayforecast.currently().d['apparentTemperature']
        
        tempDelta = apparentTempYesterday - apparentTempToday
        
        if abs(tempDelta) < 2.5:
            return 0
        elif tempDelta >= 2.5:
            return 1
        else:
            return -1
        
    def update(self):
        state = self.callWeather()
        self.callArduino(state)
        print state

